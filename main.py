import argparse
import mlrose_hiive
import os
import time
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random
from sklearn.model_selection import train_test_split
from sklearn.metrics import balanced_accuracy_score, accuracy_score
from sklearn.preprocessing import OneHotEncoder, LabelEncoder, MinMaxScaler
from experiments import fitness_experiment, problem_size_experiment, generate_hyperparam_tuning_plots

random.seed(11041996)

def run_experiments(fitness_functions):
    for fitness in fitness_functions:
        print('-------------------------')
        print("\nRunning {} Experiments\n".format(fitness['func_name']))
        print('-------------------------')
        # fitness_experiment(fitness['func_inst'], fitness['func_name'], fitness['func_name_abbr'])
        problem_size_experiment(fitness['func_inst'], fitness['func_name'], fitness['func_name_abbr'])
        # generate_hyperparam_tuning_plots(fitness['func_inst'], fitness['func_name'], fitness['func_name_abbr'])
        print('\n-------------------------')
        print("End {} Experiments\n".format(fitness['func_name']))
        print('-------------------------\n\n')

def find_optimal_rhc_nn(X_train, X_test, y_train, y_test):
    print('find_optimal_rhc_nn')
    learning_rates = [0.2, 0.1, 0.01]

    opt_accuracy = -1
    opt_lr = None
    opt_num_restarts = None
    opt_nn = None
    opt_num_nodes = None
    opt_fit_time = None
    
    one_hot = OneHotEncoder()
    y_train_hot = one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
    y_test_hot = one_hot.transform(y_test.reshape(-1, 1)).todense()
    for nodes in np.arange(10, 60, 10):
        for restarts in np.arange(0, 20, 5):
            for lr in learning_rates:
                nn = mlrose_hiive.NeuralNetwork(algorithm='random_hill_climb', hidden_nodes=[nodes], learning_rate=lr, restarts=restarts, curve=True, max_iters=1000)
                start = time.time()
                nn.fit(X_train, y_train_hot)
                end = time.time()
                y_test_pred = nn.predict(X_test)
                accuracy = balanced_accuracy_score(y_test_hot, y_test_pred)

                if accuracy > opt_accuracy:
                    print(opt_accuracy, accuracy)
                    opt_accuracy = accuracy
                    opt_lr = lr
                    opt_num_restarts = restarts
                    opt_nn = nn
                    opt_num_nodes = nodes
                    opt_fit_time = end - start
    
    print('RHC: lr={} | restarts={} | nodes={} | fit time={}'.format(opt_lr, opt_num_restarts, opt_num_nodes, opt_fit_time))
    
    # nn = mlrose_hiive.NeuralNetwork(algorithm='random_hill_climb', hidden_nodes=[60], learning_rate=opt_lr, restarts=opt_num_restarts, curve=True, max_iters=100)
    curve = opt_nn.fitness_curve
    pred = opt_nn.predict(X_test)
    accuracy = balanced_accuracy_score(y_test_hot, pred)

    print('RHC Accuracy', accuracy)

    return curve

def find_optimal_sa_nn(X_train, X_test, y_train, y_test):
    print('find_optimal_sa_nn')
    learning_rates = [0.2, 0.1, 0.01, 0.001]
    decay_rates = np.arange(0.8, 0.98, 0.02)

    opt_accuracy = -1
    opt_lr = None
    opt_decay = None
    opt_nn = None
    opt_num_nodes = None
    opt_fit_time = None
    
    one_hot = OneHotEncoder()
    y_train_hot = one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
    y_test_hot = one_hot.transform(y_test.reshape(-1, 1)).todense()
    for nodes in np.arange(10, 60, 10):
        for decay in decay_rates:
            for lr in learning_rates:
                nn = mlrose_hiive.NeuralNetwork(algorithm='simulated_annealing', hidden_nodes=[nodes], learning_rate=lr, schedule=mlrose_hiive.GeomDecay(decay=decay), curve=True, max_iters=1000)
                start = time.time()
                nn.fit(X_train, y_train_hot)
                end = time.time()
                y_test_pred = nn.predict(X_test)
                accuracy = balanced_accuracy_score(y_test_hot, y_test_pred)

                if accuracy > opt_accuracy:
                    print(accuracy, opt_accuracy)
                    opt_accuracy = accuracy
                    opt_lr = lr
                    opt_decay = decay
                    opt_nn = nn
                    opt_num_nodes = nodes
                    opt_fit_time = end - start
    
    print('SA: lr={} | decay={} | nodes={} | fit time={}'.format(opt_lr, opt_decay, opt_num_nodes, opt_fit_time))

    # nn = mlrose_hiive.NeuralNetwork(algorithm='random_hill_climb', hidden_nodes=[60], learning_rate=opt_lr, restarts=opt_num_restarts, curve=True, max_iters=100)
    curve = opt_nn.fitness_curve
    pred = opt_nn.predict(X_test)
    accuracy = balanced_accuracy_score(y_test_hot, pred)

    print('SA Accuracy', accuracy)

    return curve

def find_optimal_ga_nn(X_train, X_test, y_train, y_test):
    print('find_optimal_ga_nn')
    learning_rates = [0.2, 0.1, 0.01, 0.001]
    pop_sizes = [100, 150, 200, 250, 300]

    opt_accuracy = -1
    opt_lr = None
    opt_pop_size = None
    opt_nn = None
    opt_num_nodes = None
    opt_fit_time = None
    
    one_hot = OneHotEncoder()
    y_train_hot = one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
    y_test_hot = one_hot.transform(y_test.reshape(-1, 1)).todense()
    for nodes in np.arange(10, 60, 10):
        for pop_size in pop_sizes:
            for lr in learning_rates:
                nn = mlrose_hiive.NeuralNetwork(algorithm='genetic_alg', hidden_nodes=[nodes], learning_rate=lr, pop_size=pop_size, curve=True, max_iters=1000)
                start = time.time()
                nn.fit(X_train, y_train_hot)
                end = time.time()
                y_test_pred = nn.predict(X_test)
                accuracy = balanced_accuracy_score(y_test_hot, y_test_pred)

                if accuracy > opt_accuracy:
                    print(accuracy, opt_accuracy)
                    opt_accuracy = accuracy
                    opt_lr = lr
                    opt_pop_size = pop_size
                    opt_num_nodes = nodes
                    opt_nn = nn
                    opt_fit_time = end - start

    
    print('GA: lr={} | pop_size={} | nodes={} | fit time={}'.format(opt_lr, opt_pop_size, opt_num_nodes, opt_fit_time))

    # nn = mlrose_hiive.NeuralNetwork(algorithm='random_hill_climb', hidden_nodes=[60], learning_rate=opt_lr, restarts=opt_num_restarts, curve=True, max_iters=100)
    curve = opt_nn.fitness_curve
    pred = opt_nn.predict(X_test)
    accuracy = balanced_accuracy_score(y_test_hot, pred)

    print('GA Accuracy', accuracy)

    return curve


def neural_net_section():
    data = pd.read_csv('data/bank-additional.csv', sep=';')
    print('------------------\n-------BANK-------\n------------------')
    print(data['y'].value_counts())
    for col in data:
        if isinstance(data[col].iloc[0], str):
            le = LabelEncoder()
            le.fit(data[col])
            data[col] = le.transform(data[col])

     # Split to train/test
    train, test = train_test_split(data, test_size=0.2)
    y_train = train['y'].to_numpy()
    X_train = train.drop(['y'], axis=1).to_numpy()
    y_test = test['y'].to_numpy()
    X_test = test.drop(['y'], axis=1).to_numpy()
    print("Size of Train, Test", len(X_train), len(X_test))

     # One hot encode target values
    one_hot = OneHotEncoder()
    y_train_hot = one_hot.fit_transform(y_train.reshape(-1, 1)).todense()
    y_test_hot = one_hot.transform(y_test.reshape(-1, 1)).todense()

    # Normalize feature data
    scaler = MinMaxScaler()

    X_train_scaled = scaler.fit_transform(X_train)
    X_test_scaled = scaler.transform(X_test)

    # a1 NN
    a1_nn = mlrose_hiive.NeuralNetwork(hidden_nodes=[75], learning_rate=0.001, algorithm="gradient_descent", curve=True, max_iters=200)    
    start = time.time()
    a1_nn.fit(X_train_scaled, y_train)
    end = time.time()
    fit_time = end - start
    a1_curve = a1_nn.fitness_curve
    start = time.time()
    a1_pred = a1_nn.predict(X_test_scaled)
    end = time.time()
    pred_time = end - start
    a1_accuracy = balanced_accuracy_score(y_test, a1_pred)

    print('-----Fit Times-----')
    print('a1 fit time | query time: {} | {}'.format(fit_time, pred_time))
    

    # rhc_curve = find_optimal_rhc_nn(X_train_scaled, X_test_scaled, y_train, y_test)
    # sa_curve = find_optimal_sa_nn(X_train_scaled, X_test_scaled, y_train, y_test)
    # ga_curve = find_optimal_ga_nn(X_train_scaled, X_test_scaled, y_train, y_test)

    rhc_curves = []
    rhc_accuracy_sum = 0
    num_iters = 5
    for i in range(num_iters):
        rhc_nn = mlrose_hiive.NeuralNetwork(algorithm='random_hill_climb', hidden_nodes=[20], learning_rate=.1, restarts=15, curve=True, max_iters=200)
        start = time.time()
        rhc_nn.fit(X_train, y_train)
        end = time.time()
        fit_time = end - start
        rhc_curve = rhc_nn.fitness_curve
        
        start = time.time()
        rhc_pred = rhc_nn.predict(X_test)
        end = time.time()
        pred_time = end - start
        print('RHC fit time | query time: {} | {}'.format(fit_time, pred_time))
        rhc_accuracy = balanced_accuracy_score(y_test, rhc_pred)

        rhc_curves.append(rhc_curve[:, 0])
        rhc_accuracy_sum += rhc_accuracy
    rhc_curve = np.mean(rhc_curves, axis=0)
    
    sa_curves = []
    sa_accuracy_sum = 0
    for i in range(num_iters):
        sa_nn = mlrose_hiive.NeuralNetwork(algorithm='simulated_annealing', hidden_nodes=[40], learning_rate=.001, schedule=mlrose_hiive.GeomDecay(decay=0.8), curve=True, max_iters=200)

        start = time.time()
        sa_nn.fit(X_train, y_train)
        end = time.time()
        fit_time = end - start
        sa_curve = sa_nn.fitness_curve
        start = time.time()
        sa_pred = sa_nn.predict(X_test)
        end = time.time()
        pred_time = end - start
        print('SA fit time | query time: {} | {}'.format(fit_time, pred_time))
        sa_accuracy = balanced_accuracy_score(y_test, sa_pred)

        sa_curves.append(sa_curve[:, 0])
        sa_accuracy_sum += sa_accuracy
    sa_curves = np.array(sa_curves)
    sa_curve = np.mean(sa_curves, axis=0)


    ga_curves = []
    ga_accuracy_sum = 0
    for i in range(num_iters):
        ga_nn = mlrose_hiive.NeuralNetwork(algorithm='genetic_alg', hidden_nodes=[20], learning_rate=.01, pop_size=200, curve=True, max_iters=200)

        start = time.time()
        ga_nn.fit(X_train, y_train)
        end = time.time()
        fit_time = end - start
        ga_curve = ga_nn.fitness_curve
        start = time.time()
        ga_pred = ga_nn.predict(X_test)
        end = time.time()
        pred_time = end - start
        print('GA fit time | query time: {} | {}'.format(fit_time, pred_time))
        ga_accuracy = balanced_accuracy_score(y_test, ga_pred)

        ga_curves.append(ga_curve[:, 0])
        ga_accuracy_sum += ga_accuracy
    
    
    print('\n-----Accuracies-----')
    print('a1 accuracy: {}'.format(a1_accuracy))
    print('rhc accuracy: {}'.format(rhc_accuracy_sum / num_iters))
    print('sa accuracy: {}'.format(sa_accuracy_sum / num_iters))
    print('ga accuracy: {}'.format(ga_accuracy_sum / num_iters))
    ga_curve = np.mean(ga_curves, axis=0)


    print('a1_curve.shape', a1_curve.shape)
    print('rhc_curve.shape', rhc_curve.shape)
    print('sa_curve.shape', sa_curve.shape)
    print('ga_curve.shape', sa_curve.shape)

    plt.figure('nn loss')
    plt.plot(np.arange(len(a1_curve)), -a1_curve)
    plt.plot(np.arange(len(rhc_curve)), rhc_curve, color="red")
    plt.plot(np.arange(len(sa_curve)), sa_curve)
    plt.plot(np.arange(len(ga_curve)), ga_curve)
    plt.legend(['Backpropagation', 'RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', ])
    plt.title("NN Loss Curves")
    plt.xlabel('Iterations')
    plt.ylabel('Log Loss')
    plt.savefig('plots/nn/loss')	

    plt.figure('nn loss log')
    plt.plot(np.arange(len(a1_curve)), np.log(-a1_curve))
    plt.plot(np.arange(len(rhc_curve)), np.log(rhc_curve), color="red")
    plt.plot(np.arange(len(sa_curve)), np.log(sa_curve))
    plt.plot(np.arange(len(ga_curve)), np.log(ga_curve))
    plt.legend(['Backpropagation', 'RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', ])
    plt.title("NN Loss Curves")
    plt.xlabel('Iterations')
    plt.ylabel('Log Loss')
    plt.savefig('plots/nn/loss_log')	

    # Exclude A1

    plt.figure('nn loss no a1')
    plt.plot(np.arange(len(rhc_curve)), rhc_curve, color="red")
    plt.plot(np.arange(len(sa_curve)), sa_curve)
    plt.plot(np.arange(len(ga_curve)), ga_curve)
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', ])
    plt.title("NN Loss Curves")
    plt.xlabel('Iterations')
    plt.ylabel('Log Loss')
    plt.savefig('plots/nn/loss_no_a1')	

    plt.figure('nn loss log no a1')
    plt.plot(np.arange(len(rhc_curve)), np.log(rhc_curve), color="red")
    plt.plot(np.arange(len(sa_curve)), np.log(sa_curve))
    plt.plot(np.arange(len(ga_curve)), np.log(ga_curve))
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', ])
    plt.title("NN Loss Curves")
    plt.xlabel('Iterations')
    plt.ylabel('Log Loss')
    plt.savefig('plots/nn/loss_log_no_a1')	


def main():
    # Handle Args
    parser = argparse.ArgumentParser(description='CS7641 Assignment-2')
    parser.add_argument('--peaks', action='store_true')
    parser.add_argument('--flipflop', action='store_true')
    parser.add_argument('--knapsack', action='store_true')
    parser.add_argument('--all', action='store_true')
    # Run nn section
    parser.add_argument('--nn', action='store_true')
    args = parser.parse_args()

    fitness_functions = []
    if args.peaks or args.all:
        fitness_functions.append({'func_name_abbr': '4peaks', 'func_name': '4 Peaks', 'func_inst': mlrose_hiive.FourPeaks(t_pct=0.15)})

    if args.flipflop or args.all:
        fitness_functions.append({'func_name_abbr': 'flipflop', 'func_name': 'Flip Flop', 'func_inst': mlrose_hiive.FlipFlop() })

    if args.knapsack or args.all:
        weights = [10, 5, 2, 8, 15, 11, 4, 7, 1, 20]
        values = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        fitness_functions.append({'func_name_abbr': 'knapsack', 'func_name': 'knapsack', 'func_inst': None })

    run_experiments(fitness_functions)
    
    # If NN, run the NN stuff
    if args.nn:
        neural_net_section()

if __name__ == '__main__':
    main()
    os.system("say 'Run Completed'")
