import time
import numpy as np
import mlrose_hiive
import matplotlib.pyplot as plt

from algorithms import RandomHillClimb, SimulatedAnnealing, GeneticAlgorithm, Mimic

def fitness_experiment(func_inst, func_name, func_name_abbr):
    print('\n--fitness_experiment--\n')
    problem_size = 40
    problem = None
    if func_name_abbr is 'knapsack':
        problem = mlrose_hiive.KnapsackGenerator().generate(17, number_of_items_types=40, max_weight_pct=0.35)
    elif func_name_abbr is 'tsp':
        problem = mlrose_hiive.TSPGenerator().generate(7, 5)
    elif func_name_abbr is 'kcolor':
        problem = mlrose_hiive.MaxKColorGenerator().generate(7, 40)
    else:
        problem = mlrose_hiive.DiscreteOpt(length=problem_size, fitness_fn=func_inst, maximize=True, max_val=2) 

    problem.set_mimic_fast_mode(True)

    rhc = RandomHillClimb()
    start = time.time()
    _, _, rhc_curve, _ = rhc.tune_hyperparameters(problem)
    end = time.time()
    print('RHC Fit time: {}'.format(end - start))
    print(rhc_curve.shape)

    sa = SimulatedAnnealing()
    start = time.time()
    _, _, sa_curve, _ = sa.tune_hyperparameters(problem)
    end = time.time()
    print('SA Fit time: {}'.format(end - start))
    print(sa_curve.shape)

    ga = GeneticAlgorithm()
    start = time.time()
    _, _, ga_curve, _ = ga.tune_hyperparameters(problem)
    end = time.time()
    print('GA Fit time: {}'.format(end - start))
    print(ga_curve.shape)

    mimic = Mimic()
    start = time.time()
    _, _, mimic_curve, _ = mimic.tune_hyperparameters(problem)
    end = time.time()
    print('MIMIC Fit time: {}'.format(end - start))
    print(mimic_curve.shape)


    plt.figure('{} Fitness (problem size={})'.format(func_name, problem_size))
    plt.plot(np.arange(len(rhc_curve)), rhc_curve[:,0], color="red")
    plt.plot(np.arange(len(sa_curve)), sa_curve[:, 0])
    plt.plot(np.arange(len(ga_curve)), ga_curve[:, 0])
    plt.plot(np.arange(len(mimic_curve)), mimic_curve[:, 0])
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', 'Mimic'])
    plt.title("{} Fitness (problem size={})".format(func_name, problem_size))
    plt.xlabel('Iterations')
    plt.ylabel('Fitness')
    plt.savefig('plots/{}/fitness'.format(func_name_abbr))	

    plt.figure('{} Function Evals per Iteration (problem size={})'.format(func_name, problem_size))
    plt.plot(np.arange(len(rhc_curve)), rhc_curve[:, 1], color="red")
    plt.plot(np.arange(len(sa_curve)), sa_curve[:, 1])
    plt.plot(np.arange(len(ga_curve)), ga_curve[:, 1])
    plt.plot(np.arange(len(mimic_curve)), mimic_curve[:, 1])
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', 'Mimic'])
    plt.title('{} Function Evals per Iteration (problem size={})'.format(func_name, problem_size))
    plt.xlabel('Iterations')
    plt.ylabel('Number of Evaluations')
    plt.savefig('plots/{}/evals_per_iter'.format(func_name_abbr))

    plt.figure('{} Log of Function Evals per Iteration (problem size={})'.format(func_name, problem_size))
    plt.plot(np.arange(len(rhc_curve)), np.log(rhc_curve[:, 1]), color="red")
    plt.plot(np.arange(len(sa_curve)), np.log(sa_curve[:, 1]))
    plt.plot(np.arange(len(ga_curve)), np.log(ga_curve[:, 1]))
    plt.plot(np.arange(len(mimic_curve)), np.log(mimic_curve[:, 1]))
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', 'Mimic'])
    plt.title('{} Log of Function Evals per Iteration (problem size={})'.format(func_name, problem_size))
    plt.xlabel('Iterations')
    plt.ylabel('Number of Evaluations')
    plt.savefig('plots/{}/evals_per_iter_log'.format(func_name_abbr))		

def problem_size_experiment(func_inst, func_name, func_name_abbr):
    print('\n--problem_size_experiment--\n')

    lengths = np.arange(20, 200, 20)
    fitness_curves = {
        'rhc': [],
        'sa': [],
        'ga': [],
        'mimic': [],
    }
    clock_curves = {
        'rhc': [],
        'sa': [],
        'ga': [],
        'mimic': [],
    }
    num_evaluation_curves = {
        'rhc': [],
        'sa': [],
        'ga': [],
        'mimic': [],
    }
    for length in lengths:
        print('length:', length)
        problem = None
        if func_name_abbr is 'knapsack':
            problem = mlrose_hiive.KnapsackGenerator().generate(11041996, number_of_items_types=length)
        else:
            problem = mlrose_hiive.DiscreteOpt(length=length, fitness_fn=func_inst, maximize=True, max_val=2)
        problem.set_mimic_fast_mode(True)

        rhc = RandomHillClimb()
        best_state, best_fitness, curve, clock_time = rhc.tune_hyperparameters(problem)
        fitness_curves['rhc'].append(best_fitness)
        clock_curves['rhc'].append(clock_time)
        num_evaluation_curves['rhc'].append(np.sum(curve[:, 1]))

        sa = SimulatedAnnealing()
        best_state, best_fitness, curve, clock_time = sa.tune_hyperparameters(problem)
        fitness_curves['sa'].append(best_fitness)
        clock_curves['sa'].append(clock_time)
        num_evaluation_curves['sa'].append(np.sum(curve[:, 1]))

        ga = GeneticAlgorithm()
        best_state, best_fitness, curve, clock_time = ga.tune_hyperparameters(problem)
        fitness_curves['ga'].append(best_fitness)
        clock_curves['ga'].append(clock_time)
        num_evaluation_curves['ga'].append(np.sum(curve[:, 1]))

        mimic = Mimic()
        best_state, best_fitness, curve, clock_time = mimic.tune_hyperparameters(problem)
        fitness_curves['mimic'].append(best_fitness)
        clock_curves['mimic'].append(clock_time)
        num_evaluation_curves['mimic'].append(np.sum(curve[:, 1]))

    plt.figure('{} Fitness Change Length'.format(func_name))
    plt.plot(lengths, fitness_curves['rhc'], color="red")
    plt.plot(lengths, fitness_curves['sa'])
    plt.plot(lengths, fitness_curves['ga'])
    plt.plot(lengths, fitness_curves['mimic'])
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', 'Mimic'])
    plt.title("{} Curves Fitness with Change Problem Length".format(func_name))
    plt.xlabel('Problem Size')
    plt.ylabel('Fitness')
    plt.savefig('plots/{}/problem_length_fitness'.format(func_name_abbr))	

    plt.figure('{} Time Change Length'.format(func_name_abbr))
    plt.plot(lengths, clock_curves['rhc'], color="red")
    plt.plot(lengths, clock_curves['sa'])
    plt.plot(lengths, clock_curves['ga'])
    plt.plot(lengths, clock_curves['mimic'])
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', 'Mimic'])
    plt.title("{} Curves Wallclock Time with Change Problem Length".format(func_name))
    plt.xlabel('Problem Size')
    plt.ylabel('Time (s)')
    plt.savefig('plots/{}/problem_length_time'.format(func_name_abbr))	

    plt.figure('{} Evaluations by Problem Size'.format(func_name_abbr))
    plt.plot(lengths, num_evaluation_curves['rhc'], color="red")
    plt.plot(lengths, num_evaluation_curves['sa'])
    plt.plot(lengths, num_evaluation_curves['ga'])
    plt.plot(lengths, num_evaluation_curves['mimic'])
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', 'Mimic'])
    plt.title("{} Num Evaluations by Problem Length".format(func_name))
    plt.xlabel('Problem Length')
    plt.ylabel('Number of Evaluations')
    plt.savefig('plots/{}/num_evaluations_by_problem_size'.format(func_name_abbr))	

    plt.figure('{} Evaluations by Problem Size log'.format(func_name_abbr))
    plt.plot(lengths, np.log(num_evaluation_curves['rhc']), color="red")
    plt.plot(lengths, np.log(num_evaluation_curves['sa']))
    plt.plot(lengths, np.log(num_evaluation_curves['ga']))
    plt.plot(lengths, np.log(num_evaluation_curves['mimic']))
    plt.legend(['RandomHillClimb', 'SimulatedAnnealing', 'GeneticAlgorithm', 'Mimic'])
    plt.title("{} Num Evaluations by Problem Length".format(func_name))
    plt.xlabel('Problem Length')
    plt.ylabel('Log of Number of Evaluations')
    plt.savefig('plots/{}/num_evaluations_by_problem_size_log'.format(func_name_abbr))	

def generate_hyperparam_tuning_plots(func_inst, func_name, func_name_abbr):
    
    if func_name_abbr is '4peaks':
        rhc = RandomHillClimb()
        problem = mlrose_hiive.DiscreteOpt(length=80, fitness_fn=mlrose_hiive.FourPeaks(t_pct=0.15), maximize=True, max_val=2)
        rhc.plot_hyperparam_tuning(problem)

        ga = GeneticAlgorithm()
        problem = mlrose_hiive.DiscreteOpt(length=40, fitness_fn=mlrose_hiive.FourPeaks(t_pct=0.15), maximize=True, max_val=2)
        ga.plot_hyperparam_tuning(problem)

    elif func_name_abbr is 'queens':
        sa = SimulatedAnnealing()
        problem = mlrose_hiive.DiscreteOpt(length=40, fitness_fn=mlrose_hiive.Queens(), maximize=True, max_val=2)
        sa.plot_hyperparam_tuning(problem)

    elif func_name_abbr is 'flipflop':
        sa = SimulatedAnnealing()
        problem = mlrose_hiive.DiscreteOpt(length=500, fitness_fn=mlrose_hiive.FlipFlop(), maximize=True, max_val=2)
        sa.plot_hyperparam_tuning(problem)

    elif func_name_abbr is 'knapsack':
        mimic = Mimic()
        problem = mlrose_hiive.KnapsackGenerator().generate(17, number_of_items_types=40, max_weight_pct=0.35)
        mimic.plot_hyperparam_tuning(problem)