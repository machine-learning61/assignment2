import mlrose_hiive
import random
import time
import matplotlib.pyplot as plt
import numpy as np

from algorithms import BaseAlgorithm

max_attempts = [5, 10, 15, 20, 25, 30]
decay_rates = np.arange(0.1, 0.98, 0.02)

labels = {
    'param_1_name': 'Max Attempts',
    'param_1_key': 'max_attempts',
    'param_2_name': 'Schedules',
    'param_2_key': 'schedule',
    'alg_name': 'SimulatedAnnealing',
    'alg_name_abbr': 'sa',
}

class SimulatedAnnealing(BaseAlgorithm):
    def __init__(self):
        super().__init__(mlrose_hiive.simulated_annealing, max_attempts, decay_rates, labels)

    def tune_hyperparameters(self, problem,):
        print("Run {}".format(self.labels['alg_name']))
        optimal_param_1 = -1
        optimal_param_2 = -1
        optimal_best_fitness = -1
        optimal_curve = -1

        for param_1 in self.param_1_range:
            for decay in decay_rates:
                params = {self.labels['param_1_key']: param_1, self.labels['param_2_key']: mlrose_hiive.GeomDecay(decay=decay), }

                best_state, best_fitness, curve = self.alg(problem, **params, max_iters=5000, curve=True)
                if best_fitness > optimal_best_fitness:
                    optimal_best_fitness = best_fitness
                    optimal_param_1 = param_1
                    optimal_param_2 = mlrose_hiive.GeomDecay(decay=decay)
                    optimal_curve = curve
        
        labels = [self.labels['alg_name'], self.labels['param_1_name'], optimal_param_1, self.labels['param_2_name'], optimal_param_2]
        
        start = time.time()
        params = {self.labels['param_1_key']: optimal_param_1, self.labels['param_2_key']: optimal_param_2, }
        best_state, best_fitness, best_curve = self.alg(problem, **params, max_iters=5000, curve=True)
        end = time.time()
        
        return best_state, best_fitness, best_curve, end-start

    def plot_hyperparam_tuning(self, problem):
        print('plotting hyperparam tuning')
        curve = []
        for param_1 in self.param_1_range:
            fitness_sum = 0
            params = { self.labels['param_1_key']: param_1 }
            for i in range(10):
                random.seed(i)
                _, best_fitness, _ = self.alg(problem, **params, max_iters=100, curve=True)
                fitness_sum += best_fitness
            curve.append(fitness_sum / 10)

        plt.figure('{} Param 1'.format(self.labels['alg_name']))
        plt.plot(self.param_1_range, curve, color="red")
        plt.title("{} {} Tuning".format(self.labels['alg_name'], self.labels['param_1_name']))
        plt.xlabel(self.labels['param_1_name'])
        plt.ylabel('Fitness')
        plt.savefig('plots/{}/{}'.format(self.labels['alg_name_abbr'], self.labels['param_1_key']))	

        curve = []
        fit_time_curve = []
        for decay in decay_rates:
            print(decay)
            fitness_sum = 0
            fit_time_sum = 0
            for i in range(10):
                random.seed(i)
                start = time.time()
                _, best_fitness, _ = self.alg(problem, schedule=mlrose_hiive.GeomDecay(decay=decay), max_iters=100, curve=True)
                end = time.time()
                fitness_sum += best_fitness
                fit_time_sum += (end-start)
            curve.append(fitness_sum/10)
            fit_time_curve.append(fit_time_sum/10)
        
        plt.figure('{} Param 2'.format(self.labels['alg_name']))
        plt.plot(decay_rates, curve, color="red")
        plt.title("{} {} Tuning".format(self.labels['alg_name'], 'Decay Rate'))
        plt.xlabel('Decay Rate')
        plt.ylabel('Fitness')
        plt.savefig('plots/{}/{}'.format(self.labels['alg_name_abbr'], self.labels['param_2_key']))	

        plt.figure('SA Fit Time by Decay'.format(self.labels['alg_name']))
        plt.plot(decay_rates, fit_time_curve, color="red")
        plt.title("SA Decay Tuning")
        plt.xlabel('Decay Rate')
        plt.ylabel('Time to Converge')
        plt.savefig('plots/sa/decay_rate_fit_time')	

        return