import mlrose_hiive
import numpy as np
from algorithms import BaseAlgorithm
import time
import random
import numpy as np
import matplotlib.pyplot as plt

max_attempts = np.arange(0, 55, 5)
pop_sizes = np.arange(50, 550, 50)
labels = {
    'param_1_name': 'Max Attempts',
    'param_1_key': 'max_attempts',
    'param_2_name': 'Population Sizes',
    'param_2_key': 'pop_size',
    'alg_name': 'GeneticAlgorithm',
    'alg_name_abbr': 'ga',
}

class GeneticAlgorithm(BaseAlgorithm):
    def __init__(self):
        super().__init__(mlrose_hiive.genetic_alg, max_attempts, pop_sizes, labels)

    def plot_hyperparam_tuning(self, problem):
        print('plotting hyperparam tuning')
        curve = []
        # for param_1 in self.param_1_range:
        #     fitness_sum = 0
        #     params = { self.labels['param_1_key']: param_1 }
        #     for i in range(10):
        #         random.seed(i)
        #         _, best_fitness, _ = self.alg(problem, **params, max_iters=100, curve=True)
        #         fitness_sum += best_fitness
        #     curve.append(fitness_sum / 10)

        # plt.figure('{} Param 1'.format(self.labels['alg_name']))
        # plt.plot(self.param_1_range, curve, color="red")
        # plt.title("{} {} Tuning".format(self.labels['alg_name'], self.labels['param_1_name']))
        # plt.xlabel(self.labels['param_1_name'])
        # plt.ylabel('Fitness')
        # plt.savefig('plots/{}/{}'.format(self.labels['alg_name_abbr'], self.labels['param_1_key']))	


        # curve = []
        # for param_2 in self.param_2_range:
        #     fitness_sum = 0
        #     params = { self.labels['param_2_key']: param_2 }
        #     for i in range(10):
        #         random.seed(i)
        #         _, best_fitness, _ = self.alg(problem, **params, max_iters=100, curve=True)
        #         fitness_sum += best_fitness
        #     curve.append(fitness_sum/10)
        
        # plt.figure('{} Param 2'.format(self.labels['alg_name']))
        # plt.plot(self.param_2_range, curve, color="red")

            
        # plt.title("{} {} Tuning".format(self.labels['alg_name'], self.labels['param_2_name']))
        # plt.xlabel(self.labels['param_2_name'])
        # plt.ylabel('Fitness')
        # plt.savefig('plots/{}/{}'.format(self.labels['alg_name_abbr'], self.labels['param_2_key']))	

        curves_by_problem_size = {}
        fit_time_by_problem_size = {}
        for prob_size in np.arange(20, 100, 20):
            curves_by_problem_size[prob_size] = []
            fit_time_by_problem_size[prob_size] = []
            new_problem = mlrose_hiive.DiscreteOpt(length=prob_size, fitness_fn=mlrose_hiive.FourPeaks(t_pct=0.15), maximize=True, max_val=2)
            # Pop size
            for param_2 in self.param_2_range:
                fitness_sum = 0
                fit_time_sum = 0
                params = { self.labels['param_2_key']: param_2 }
                for i in range(10):
                    random.seed(i)
                    start = time.time()
                    _, best_fitness, _ = self.alg(new_problem, **params, max_iters=100, curve=True)
                    end = time.time()
                    fitness_sum += best_fitness
                    fit_time_sum += (end - start)
                curves_by_problem_size[prob_size].append(fitness_sum/10)
                fit_time_by_problem_size[prob_size].append(fit_time_sum/10)
            
        plt.figure('{} Param 2'.format(self.labels['alg_name']))
        legend = []
        for idx, val in enumerate(curves_by_problem_size):
            plt.plot(self.param_2_range, curves_by_problem_size[val], '-o', label='Problem Size: ' + str(val))
            legend.append('Problem Size: ' + str(val))
            
        plt.title("Pop Size Tuning by Problem Length")
        plt.legend(legend)
        plt.xlabel(self.labels['param_2_name'])
        plt.ylabel('Fitness')
        plt.savefig('plots/ga/pop_size_problem_length')	

        plt.figure('fit time boiii')
        legend = []
        for idx, val in enumerate(fit_time_by_problem_size):
            plt.plot(self.param_2_range, fit_time_by_problem_size[val], '-o', label='Problem Size: ' + str(val))
            legend.append('Problem Size: ' + str(val))
            
        plt.title("Pop Size Tuning by Problem Length, Converge Time")
        plt.legend(legend)
        plt.xlabel(self.labels['param_2_name'])
        plt.ylabel('Time to Converge (s)')
        plt.savefig('plots/ga/pop_size_problem_length_fit_time')	
        return