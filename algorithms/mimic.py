import mlrose_hiive
import numpy as np
from algorithms import BaseAlgorithm
import random
import time
import matplotlib.pyplot as plt

max_attempts = np.arange(0, 55, 5)
pop_sizes = [50, 150, 250]
keep_pcts = np.arange(.1, .55, .05)
# keep_pcts = [0.1, 0.15, 0.2, 0.25, 0.3]
labels = {
    'param_1_name': 'Max Attempts',
    'param_1_key': 'max_attempts',
    'param_2_name': 'Population Sizes',
    'param_2_key': 'pop_size',
    'alg_name': 'Mimic',
    'alg_name_abbr': 'mimic',
}

class Mimic(BaseAlgorithm):
    def __init__(self):
        super().__init__(mlrose_hiive.mimic, max_attempts, pop_sizes, labels)
    
    def plot_hyperparam_tuning(self, problem):
        print('plotting hyperparam tuning')

        fitness_by_problem_size = {}
        fit_time_by_problem_size = {}
        for prob_size in np.arange(20, 80, 20):
            print(fitness_by_problem_size)
            fit_time_by_problem_size[prob_size] = []
            fitness_by_problem_size[prob_size] = []
            new_problem = mlrose_hiive.KnapsackGenerator().generate(17, number_of_items_types=prob_size, max_weight_pct=0.35)

            for keep_pct in keep_pcts:
                # Pop size
                fitness_sum = 0
                fit_time_sum = 0
                for i in range(10):
                    random.seed(i)
                    start = time.time()
                    _, best_fitness, _ = self.alg(new_problem, keep_pct=keep_pct, max_iters=100, curve=True)
                    end = time.time()
                    fitness_sum += best_fitness
                    fit_time_sum += (end - start)
                fitness_by_problem_size[prob_size].append(fitness_sum/10)
                fit_time_by_problem_size[prob_size].append(fit_time_sum/10)


   

            
        plt.figure('MIMIC keep_pct')
        legend = []
        print(fitness_by_problem_size)
        for idx, val in enumerate(fitness_by_problem_size):
            plt.plot(keep_pcts, fitness_by_problem_size[val], '-o', label='Problem Size: ' + str(val))
            legend.append('Problem Size: ' + str(val))
        plt.legend(legend)
        plt.title("Fitness by Keep Pct")
        plt.xlabel('Keep Pct')
        plt.ylabel('Fitness')
        plt.savefig('plots/mimic/keep_pct_fitness')	

        plt.figure('MIMIC keep_pct time')
        legend = []
        for idx, val in enumerate(fit_time_by_problem_size):
            plt.plot(keep_pcts, fit_time_by_problem_size[val], '-o', label='Problem Size: ' + str(val))
            legend.append('Problem Size: ' + str(val))
        plt.title("Time to converge by Keep Pct")
        plt.legend(legend)
        plt.xlabel('Keep Pct')
        plt.ylabel('Time (s)')
        plt.savefig('plots/mimic/keep_pct_time')	

        plt.figure('MIMIC keep_pct gradient')
        legend = []
        print(fitness_by_problem_size)
        for idx, val in enumerate(fitness_by_problem_size):
            plt.plot(keep_pcts, np.gradient(fitness_by_problem_size[val]), '-o', label='Problem Size: ' + str(val))
            legend.append('Problem Size: ' + str(val))
        plt.legend(legend)
        plt.title("Fitness by Keep Pct")
        plt.xlabel('Keep Pct')
        plt.ylabel('Fitness')
        plt.savefig('plots/mimic/keep_pct_fitness_gradient')	

        plt.figure('MIMIC keep_pct time gradient')
        legend = []
        for idx, val in enumerate(fit_time_by_problem_size):
            plt.plot(keep_pcts, np.gradient(fit_time_by_problem_size[val]), '-o', label='Problem Size: ' + str(val))
            legend.append('Problem Size: ' + str(val))
        plt.title("Time to converge by Keep Pct")
        plt.legend(legend)
        plt.xlabel('Keep Pct')
        plt.ylabel('Time (s)')
        plt.savefig('plots/mimic/keep_pct_time_gradient')	

        
        # curves_by_problem_size = {}
        # fit_time_by_problem_size = {}
        # for prob_size in np.arange(20, 100, 20):
        #     curves_by_problem_size[prob_size] = []
        #     fit_time_by_problem_size[prob_size] = []
        #     new_problem = mlrose_hiive.DiscreteOpt(length=prob_size, fitness_fn=mlrose_hiive.FourPeaks(t_pct=0.15), maximize=True, max_val=2)
        #     # Pop size
        #     for param_2 in self.param_2_range:
        #         fitness_sum = 0
        #         fit_time_sum = 0
        #         params = { self.labels['param_2_key']: param_2 }
        #         for i in range(10):
        #             random.seed(i)
        #             start = time.time()
        #             _, best_fitness, _ = self.alg(new_problem, **params, max_iters=100, curve=True)
        #             end = time.time()
        #             fitness_sum += best_fitness
        #             fit_time_sum += (end - start)
        #         curves_by_problem_size[prob_size].append(fitness_sum/10)
        #         fit_time_by_problem_size[prob_size].append(fit_time_sum/10)
            
        # plt.figure('{} Param 2'.format(self.labels['alg_name']))
        # legend = []
        # for idx, val in enumerate(curves_by_problem_size):
        #     plt.plot(self.param_2_range, curves_by_problem_size[val], '-o', label='Problem Size: ' + str(val))
        #     legend.append('Problem Size: ' + str(val))
            
        # plt.title("Pop Size Tuning by Problem Length")
        # plt.legend(legend)
        # plt.xlabel(self.labels['param_2_name'])
        # plt.ylabel('Fitness')
        # plt.savefig('plots/mimic/pop_size_problem_length')	

        # plt.figure('fit time boiii')
        # legend = []
        # for idx, val in enumerate(fit_time_by_problem_size):
        #     plt.plot(self.param_2_range, fit_time_by_problem_size[val], '-o', label='Problem Size: ' + str(val))
        #     legend.append('Problem Size: ' + str(val))
            
        # plt.title("Pop Size Tuning by Problem Length, Converge Time")
        # plt.legend(legend)
        # plt.xlabel(self.labels['param_2_name'])
        # plt.ylabel('Time to Converge (s)')
        # plt.savefig('plots/mimic/pop_size_problem_length_fit_time')	

        

        return