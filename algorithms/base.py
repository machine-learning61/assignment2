import time
import random
import numpy as np
import matplotlib.pyplot as plt

class BaseAlgorithm(object):
    def __init__(self, alg, param_1_range, param_2_range, labels):
        self.alg = alg
        self.param_1_range = param_1_range
        self.param_2_range = param_2_range

        '''
        {
            param_1_name,
            param_1_key,
            param_2_name,
            param_2_key,
            alg_name,
            alg_name_short
        }
        '''
        self.labels = labels


    def tune_hyperparameters(self, problem):
        print("Run {}".format(self.labels['alg_name']))
        optimal_param_1 = -1
        optimal_param_2 = -1
        optimal_best_fitness = -1
        optimal_curve = []

        # FOr some reason the curve sometimes is empty...
        while len(optimal_curve) == 0:
            print(optimal_curve)
            for param_1 in self.param_1_range:
                for param_2 in self.param_2_range:
                    params = {self.labels['param_1_key']: param_1, self.labels['param_2_key']: param_2, }

                    best_state, best_fitness, curve = self.alg(problem, **params, curve=True, max_iters=500)

                    if best_fitness > optimal_best_fitness:
                        optimal_best_fitness = best_fitness
                        optimal_param_1 = param_1
                        optimal_param_2 = param_2
                        optimal_curve = curve
        
        labels = [self.labels['alg_name'], self.labels['param_1_name'], optimal_param_1, self.labels['param_2_name'], optimal_param_2]
        
        start = time.time()
        params = {self.labels['param_1_key']: optimal_param_1, self.labels['param_2_key']: optimal_param_2, }
        best_state, best_fitness, best_curve = self.alg(problem, **params, curve=True, max_iters=500)
        end = time.time()
        return best_state, best_fitness, optimal_curve, end-start
    
    def plot_hyperparam_tuning(self, problem):
        print('plotting hyperparam tuning')
        curve = []
        for param_1 in self.param_1_range:
            fitness_sum = 0
            params = { self.labels['param_1_key']: param_1 }
            for i in range(10):
                random.seed(i)
                _, best_fitness, _ = self.alg(problem, **params, max_iters=100, curve=True)
                fitness_sum += best_fitness
            curve.append(fitness_sum / 10)

        plt.figure('{} Param 1'.format(self.labels['alg_name']))
        plt.plot(self.param_1_range, curve, color="red")
        plt.title("{} {} Tuning".format(self.labels['alg_name'], self.labels['param_1_name']))
        plt.xlabel(self.labels['param_1_name'])
        plt.ylabel('Fitness')
        plt.savefig('plots/{}/{}'.format(self.labels['alg_name_abbr'], self.labels['param_1_key']))	

        curve = []
        for param_2 in self.param_2_range:
            fitness_sum = 0
            params = { self.labels['param_2_key']: param_2 }
            for i in range(10):
                random.seed(i)
                _, best_fitness, _ = self.alg(problem, **params, max_iters=100, curve=True)
                fitness_sum += best_fitness
            curve.append(fitness_sum/10)
        
        plt.figure('{} Param 2'.format(self.labels['alg_name']))
        plt.plot(self.param_2_range, curve, color="red")

            
        plt.title("{} {} Tuning".format(self.labels['alg_name'], self.labels['param_2_name']))
        plt.xlabel(self.labels['param_2_name'])
        plt.ylabel('Fitness')
        plt.savefig('plots/{}/{}'.format(self.labels['alg_name_abbr'], self.labels['param_2_key']))	

        return