import mlrose_hiive
import numpy as np
from algorithms import BaseAlgorithm

max_attempts = np.arange(0, 55, 5)
max_restarts = [0, 5, 10, 15, 20, 25, 30, 35, 40]
labels = {
    'param_1_name': 'Max Attempts',
    'param_1_key': 'max_attempts',
    'param_2_name': 'Restarts',
    'param_2_key': 'restarts',
    'alg_name': 'RandomHillClimb',
    'alg_name_abbr': 'rhc',
}

class RandomHillClimb(BaseAlgorithm):
    def __init__(self):
        super().__init__(mlrose_hiive.random_hill_climb, max_attempts, max_restarts, labels)
