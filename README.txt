Link to code repo: https://gitlab.com/machine-learning61/assignment2

Links to Datasets:
    Bank Marketing: http://archive.ics.uci.edu/ml/datasets/Bank+Marketing#

3rd Party Libraries Used:
- Sklearn: https://scikit-learn.org/
- MLRose_hiive: https://pypi.org/project/mlrose-hiive/

Python version used: Python 3.9.13

How to run Code:
    1. Create virtual environment: python3 -m venv env
    2. Activate virtual environment: source env/bin/activate
    3. Install dependencies: pip install -r requirements.txt
    4. Run code: python main.py --all --nn
        Args: 
            --all: run experiments for "all" the optimization problems
            --nn: run the Neural Network experiments
            